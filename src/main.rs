use std::fs::File;
use std::io::{Read, Write, Seek, SeekFrom};
use std::process::Command;

const URL: &str = "https://f4se.silverlock.org/";
const TARGET_FOLDER: &str = "f4se";

const MAIN_SLN: &str = "f4se/src/f4se/f4se.sln";

const PROJECT_FILES: &[&str] = &["f4se/src/common/common_vc11.vcxproj", "f4se/src/f4se/f4se/f4se.vcxproj",
    "f4se/src/f4se/f4se_common/f4se_common.vcxproj", "f4se/src/f4se/f4se_loader/f4se_loader.vcxproj", 
    "f4se/src/f4se/f4se_loader_common/f4se_loader_common.vcxproj", "f4se/src/f4se/f4se_steam_loader/f4se_steam_loader.vcxproj"];

const CMAKE_FILES: &[&str] = &["f4se/src/f4se/CMakeLists.txt", "f4se/src/f4se/f4se/CMakeLists.txt",
    "f4se/src/f4se/f4se_common/CMakeLists.txt", "f4se/src/f4se/f4se_loader/CMakeLists.txt",
    "f4se/src/f4se/f4se_loader_common/CMakeLists.txt", "f4se/src/f4se/f4se_steam_loader/CMakeLists.txt",
    "f4se/src/common/CMakeLists.txt"];

fn main() {
    let archive_url = get_archive_url();

    let archive_file = download_archive(archive_url);

    unpack_archive(&archive_file);
    delete_archive(&archive_file);

    patch_sln();

    convert_to_cmake();

    patch_cmake();
}

fn get_archive_url() -> String {
    URL.to_string() +
        &std::env::args()
            .nth(1)
            .map(|s| "beta/f4se_".to_string() + &s + ".7z")
            .unwrap_or_else(|| get_latest_version())
}

fn get_latest_version() -> String {
    let response = ureq::get(URL).call();

    let body = response.into_string().expect("Expected to be able to get page");

    let runtime_start = body.find("Fallout 4 runtime").unwrap();
    let href_start = (&body[runtime_start..]).find("href").unwrap() + runtime_start;
    let version_start = (&body[href_start..]).find("\"").unwrap() + href_start + 1;
    let version_end = (&body[version_start..]).find("\"").unwrap() + version_start;

    let version = (&body[version_start..version_end]).trim().to_string();

    println!("Found latest file to be: '{}'", version);

    version
}

fn download_archive(archive_url: String) -> String {
    let archive_name = &archive_url[archive_url.rfind('/').unwrap() + 1..];

    let response = ureq::get(&archive_url).call();

    let mut bytes = Vec::new();
    let _size = response.into_reader().read_to_end(&mut bytes).unwrap();

    let mut file = File::create(archive_name).unwrap();
    file.write_all(&bytes[..]).unwrap();

    println!("Downloaded temp archive to: '{}'", archive_name);

    archive_name.to_string()
}

fn unpack_archive(archive_file: &String) {
    if let Ok(d) = std::fs::read_dir(TARGET_FOLDER) {
        for e in d {
            if let Ok(d) = e {
                if let Ok(m) = d.metadata() {
                    if m.permissions().readonly() {
                        let mut p = m.permissions();
                        p.set_readonly(false);

                        std::fs::set_permissions(d.path(), p).unwrap();
                    }
                }
            }
        }

        std::fs::remove_dir_all(TARGET_FOLDER).unwrap();
    }

    let output = Command::new("7z.exe")
        .arg("x")
        .arg("-spe")
        // .arg(format!("-o{}", TARGET_FOLDER))
        .arg(&archive_file)
        .output()
        .unwrap();

    if !output.status.success() {
        let s = String::from_utf8_lossy(&output.stderr);
        println!("stderr:{}", s);
    } else {
        println!("Unpacked archive to folder: '{}'", TARGET_FOLDER);
    }

    std::fs::rename(archive_file.trim_end_matches(".7z"), TARGET_FOLDER).unwrap();
}

fn delete_archive(archive_file: &String) {
    std::fs::remove_file(archive_file).unwrap();

    println!("Deleted temp archive at: '{}'", archive_file);
}

fn patch_sln() {
    let mut options = std::fs::OpenOptions::new();
    options.create(false);
    options.write(true);
    options.read(true);
    options.append(false);

    for project_file in PROJECT_FILES {
        let mut file = options.open(*project_file).expect(&format!("Couldn't load {}", *project_file));

        let mut buf = String::new();
        file.read_to_string(&mut buf).unwrap();

        // buf = buf.replace("MultiThreadedDebug", "MultiThreadedDebugDLL");
        // buf = buf.replace("MultiThreaded", "MultiThreadedDLL");
        // buf = buf.replace("DynamicLibrary", "StaticLibrary");

        buf = buf.replace("MultiThreadedDebug", "$$$$$TEMP#####");
        buf = buf.replace("MultiThreaded", "MultiThreadedDLL");
        buf = buf.replace("$$$$$TEMP#####", "MultiThreadedDebugDLL");
        buf = buf.replace("DynamicLibrary", "StaticLibrary");

        file.set_len(0).unwrap();
        file.seek(SeekFrom::Start(0)).unwrap();

        file.write_all(buf.as_bytes()).unwrap();
    }

    println!("Successfully patched the sln project files");
}

fn convert_to_cmake() {
    let output = Command::new("cmake-converter.exe")
        .arg("-s")
        .arg(MAIN_SLN)
        .output()
        .unwrap();

    if !output.status.success() {
        let s = String::from_utf8_lossy(&output.stderr);
        println!("stderr:{}", s);
    } else {
        println!("Successfully converted to cmake");
    }
}

const PATCH: &[u8] = b"# PATCH
if(MSVC)
    add_definitions(/FI\"common/IPrefix.h\")
    add_definitions(/MP)
endif()\n";

fn patch_cmake() {
    let mut options = std::fs::OpenOptions::new();
    options.create(false);
    options.write(true);
    options.append(true);

    for cmake_file in CMAKE_FILES {
        let mut file = options.open(*cmake_file).expect(&format!("Couldn't load {}", *cmake_file));

        file.write_all(PATCH).unwrap();
    }

    println!("Successfully patched the cmake files");
}