This small script program will download either the latest, or a specified version of f4se (non-VR),
then convert it into a cmake project, for use in CLion.

If you wish to specify the version, you do so by passing into the first parameter, where the version is specified in the form of '0_06_21'

Requirements to run (all need to be in your PATH):
* 7-Zip
* cmake converter (https://github.com/pavelliavonau/cmakeconverter)
    * Which in turn requires python

Then if you want to actually use the cmake part you require CMake 3.15 (or greater) 